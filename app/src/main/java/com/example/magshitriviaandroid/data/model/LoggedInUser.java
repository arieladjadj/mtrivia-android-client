package com.example.magshitriviaandroid.data.model;

import java.net.Socket;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class LoggedInUser {

    private String userId;
    private String displayName;
    private Socket serverSocket;

    public LoggedInUser(String userId, String displayName, Socket serverSocket) {
        this.userId = userId;
        this.displayName = displayName;
        this.serverSocket = serverSocket;
    }

    public String getUserId() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }
}
