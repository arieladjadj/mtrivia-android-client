package com.example.magshitriviaandroid.data.protocol;

import com.example.magshitriviaandroid.data.structs.LoginRequest;
import com.example.magshitriviaandroid.data.structs.LoginResponse;
import com.google.gson.Gson;

public class JsonResponsePacketDeserializer {

    public static LoginResponse deserializeLoginResponse(byte[] buffer) {

        int jsonDataLength = ((buffer[1] << 8 | buffer[2]) << 8 | buffer[3]) << 8 | buffer[4]; //convert 4 bytes that represtns the
        String jsonString = buffer.toString();
        jsonString = jsonString.substring(5,jsonDataLength);

        Gson gson = new Gson(); // Or use new GsonBuilder().create();
        LoginResponse loginResponse = gson.fromJson(jsonString, LoginResponse.class); // deserializes json into target2
        return loginResponse;
    }

}
