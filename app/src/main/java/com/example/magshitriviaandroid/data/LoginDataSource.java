package com.example.magshitriviaandroid.data;

import android.util.Log;

import com.example.magshitriviaandroid.R;
import com.example.magshitriviaandroid.data.model.LoggedInUser;
import com.example.magshitriviaandroid.data.protocol.JsonRequestPacketSerializer;
import com.example.magshitriviaandroid.data.protocol.JsonResponsePacketDeserializer;
import com.example.magshitriviaandroid.data.structs.LoginRequest;
import com.example.magshitriviaandroid.data.structs.LoginResponse;
import com.example.magshitriviaandroid.data.structs.ResponseResult;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: handle loggedInUser authentication
            LoginRequest loginRequest = new LoginRequest(username, password);
            byte[] buffer = JsonRequestPacketSerializer.serializeRequest(loginRequest);

            //buffer = "ariel".getBytes();

            Log.d("ariel", "Create server socket");
            InetSocketAddress sockAdr = new InetSocketAddress(ServerConfig.server_ip, ServerConfig.server_port);
            Socket serverSocket = new Socket();

            serverSocket.connect(sockAdr, 5000);

                Log.d("ariel", "Connected.");
            OutputStream socketOutputStream = serverSocket.getOutputStream();
            socketOutputStream.write(buffer);

            DataInputStream dIn = new DataInputStream(serverSocket.getInputStream());

            int length = dIn.readInt();                    // read length of incoming message

            if(length <= 0) return new Result.Error(new IOException("Error logging in"));


            byte[] message = new byte[length];
            dIn.readFully(message, 0, message.length); // read the message


            LoginResponse loginResponse = JsonResponsePacketDeserializer.deserializeLoginResponse(message) ;

            if(loginResponse.status != 1) {
                serverSocket.close();
                return new Result.Error(new IOException("Invalid user name or password"));
            }

            LoggedInUser user =
                    new LoggedInUser(
                            java.util.UUID.randomUUID().toString(),
                            username, serverSocket);

            return new Result.Success<>(user);
        }catch (SocketException e){
            return new Result.ServerError(new Exception("Server error",e));
        } catch (Exception e) {
            Log.d("ariel", "exception" + e);
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}
